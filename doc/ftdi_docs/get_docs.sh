#! /bin/bash

PATH_TO_DIR=$(realpath $(dirname $0))

# ftdi d2xx programmres guide
wget https://www.ftdichip.com/Support/Documents/ProgramGuides/D2XX_Programmer%27s_Guide\(FT_000071\).pdf -O $PATH_TO_DIR/d2xx_programmers_guide.pdf

# ftdi user aread usage guide
wget https://www.ftdichip.com/Documents/AppNotes/AN_121_FTDI_Device_EEPROM_User_Area_Usage.pdf -O $PATH_TO_DIR/AN_121_FTDI_Device_EEPROM_User_Area_Usage.pdf

# ftdi mpsse doc
wget https://www.ftdichip.com/Documents/AppNotes/AN_135_MPSSE_Basics.pdf -O $PATH_TO_DIR/AN_135_MPSSE_Basics.pdf
