#! /bin/bash

PATH_TO_DIR=$(realpath $(dirname $0))

# Транслятор уровней (8 каналов)
wget https://www.ti.com/lit/ds/symlink/txs0108e.pdf\?ts=1628574370324\&ref_url=https%253A%252F%252Fwww.google.com%252F -O $PATH_TO_DIR/txs0108e.pdf

# Транслятор 4 канала (JTAG)
wget https://www.ti.com/lit/ds/symlink/sn74axc4t774.pdf\?ts=1628579219849\&ref_url=https%253A%252F%252Fwww.google.com%252F -O $PATH_TO_DIR/sn74axc4t774.pdf
