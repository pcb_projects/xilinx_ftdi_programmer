#! /bin/bash

PATH_TO_DIR=$(realpath $(dirname $0))

# LDO преобразователь напряжения
wget https://www.ti.com/lit/ds/symlink/tlv1117.pdf\?ts=1628257970259\&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTLV1117%253FHQS%253DTI-null-null-octopart-df-pf-null-wwe -O $PATH_TO_DIR/tlv1117-xx.pdf

# защита от статики
wget https://www.ti.com/lit/ds/symlink/tpd4s012.pdf\?ts=1628535838093\&ref_url=https%253A%252F%252Fms.componentsearchengine.com%252F -O $PATH_TO_DIR/tpd4s012.pdf
