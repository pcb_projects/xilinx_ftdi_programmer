Документация на проект пограмматора на базе FT4232HL
###############################################################################

.. contents::

Заметки
==========

* `Сегментация EEPROM-памяти FTDI (kit-e)`_
* `Ветка electronix посвященная ftdi jtag`_
* `Статья на хабре про конфигурацию ftdi`_


.. ----------------------------------------------------------------------------

.. _`Сегментация EEPROM-памяти FTDI (kit-e)`:
   https://kit-e.ru/memory/ft232h/

.. _`Ветка electronix посвященная ftdi jtag`:
   https://electronix.ru/forum/index.php?app=forums&module=forums&controller=topic&id=114633&page=7&app=forums&module=forums&id=114633

.. _`Статья на хабре про конфигурацию ftdi`:
   https://habr.com/ru/post/531458/
