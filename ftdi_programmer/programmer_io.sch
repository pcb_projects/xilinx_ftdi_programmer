EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Logic_LevelTranslator:TXS0108EPW U?
U 1 1 611F1382
P 7750 4950
AR Path="/611F1382" Ref="U?"  Part="1" 
AR Path="/611E7A25/611F1382" Ref="U3"  Part="1" 
F 0 "U3" H 8000 4250 50  0000 C CNN
F 1 "TXS0108EPW" H 8200 4150 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 7750 4200 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/txs0108e.pdf" H 7750 4850 50  0001 C CNN
	1    7750 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611F1388
P 7400 4150
AR Path="/611F1388" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F1388" Ref="C8"  Part="1" 
F 0 "C8" H 7100 4250 50  0000 L CNN
F 1 "0.1uF" H 7100 4150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7438 4000 50  0001 C CNN
F 3 "~" H 7400 4150 50  0001 C CNN
	1    7400 4150
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 611F1394
P 8100 4150
AR Path="/611F1394" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F1394" Ref="C10"  Part="1" 
F 0 "C10" H 7800 4250 50  0000 L CNN
F 1 "0.1uF" H 7800 4150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 8138 4000 50  0001 C CNN
F 3 "~" H 8100 4150 50  0001 C CNN
	1    8100 4150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611F139A
P 8350 4250
AR Path="/611F139A" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F139A" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 8350 4000 50  0001 C CNN
F 1 "GND" H 8355 4077 50  0000 C CNN
F 2 "" H 8350 4250 50  0001 C CNN
F 3 "" H 8350 4250 50  0001 C CNN
	1    8350 4250
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 611F13A0
P 7650 3900
AR Path="/611F13A0" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F13A0" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 7650 3750 50  0001 C CNN
F 1 "VDD" H 7665 4073 50  0000 C CNN
F 2 "" H 7650 3900 50  0001 C CNN
F 3 "" H 7650 3900 50  0001 C CNN
	1    7650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 5650 7750 5750
$Comp
L power:GND #PWR?
U 1 1 611F13AD
P 7750 5750
AR Path="/611F13AD" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F13AD" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 7750 5500 50  0001 C CNN
F 1 "GND" H 7755 5577 50  0000 C CNN
F 2 "" H 7750 5750 50  0001 C CNN
F 3 "" H 7750 5750 50  0001 C CNN
	1    7750 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3900 7650 4150
Wire Wire Line
	7550 4150 7650 4150
Connection ~ 7650 4150
Wire Wire Line
	7650 4150 7650 4250
Wire Wire Line
	7250 4150 7150 4150
Wire Wire Line
	7150 4150 7150 4250
Wire Wire Line
	7850 4150 7950 4150
Connection ~ 7850 4150
Wire Wire Line
	7850 4150 7850 4250
Wire Wire Line
	8250 4150 8350 4150
Wire Wire Line
	8350 4150 8350 4250
$Comp
L Logic_LevelTranslator:TXS0108EPW U?
U 1 1 611F13F7
P 7750 2000
AR Path="/611F13F7" Ref="U?"  Part="1" 
AR Path="/611E7A25/611F13F7" Ref="U2"  Part="1" 
F 0 "U2" H 8000 1300 50  0000 C CNN
F 1 "TXS0108EPW" H 8200 1200 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 7750 1250 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/txs0108e.pdf" H 7750 1900 50  0001 C CNN
	1    7750 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611F13FD
P 7400 1200
AR Path="/611F13FD" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F13FD" Ref="C7"  Part="1" 
F 0 "C7" H 7100 1300 50  0000 L CNN
F 1 "0.1uF" H 7100 1200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7438 1050 50  0001 C CNN
F 3 "~" H 7400 1200 50  0001 C CNN
	1    7400 1200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611F1403
P 7150 1300
AR Path="/611F1403" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F1403" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 7150 1050 50  0001 C CNN
F 1 "GND" H 7155 1127 50  0000 C CNN
F 2 "" H 7150 1300 50  0001 C CNN
F 3 "" H 7150 1300 50  0001 C CNN
	1    7150 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611F1409
P 8100 1200
AR Path="/611F1409" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F1409" Ref="C9"  Part="1" 
F 0 "C9" H 7800 1300 50  0000 L CNN
F 1 "0.1uF" H 7800 1200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 8138 1050 50  0001 C CNN
F 3 "~" H 8100 1200 50  0001 C CNN
	1    8100 1200
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611F140F
P 8350 1300
AR Path="/611F140F" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F140F" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 8350 1050 50  0001 C CNN
F 1 "GND" H 8355 1127 50  0000 C CNN
F 2 "" H 8350 1300 50  0001 C CNN
F 3 "" H 8350 1300 50  0001 C CNN
	1    8350 1300
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 611F1415
P 7650 950
AR Path="/611F1415" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F1415" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 7650 800 50  0001 C CNN
F 1 "VDD" H 7665 1123 50  0000 C CNN
F 2 "" H 7650 950 50  0001 C CNN
F 3 "" H 7650 950 50  0001 C CNN
	1    7650 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2700 7750 2800
$Comp
L power:GND #PWR?
U 1 1 611F1422
P 7750 2800
AR Path="/611F1422" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F1422" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 7750 2550 50  0001 C CNN
F 1 "GND" H 7755 2627 50  0000 C CNN
F 2 "" H 7750 2800 50  0001 C CNN
F 3 "" H 7750 2800 50  0001 C CNN
	1    7750 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 950  7650 1200
Wire Wire Line
	7550 1200 7650 1200
Connection ~ 7650 1200
Wire Wire Line
	7650 1200 7650 1300
Wire Wire Line
	7250 1200 7150 1200
Wire Wire Line
	7150 1200 7150 1300
Wire Wire Line
	7850 1200 7950 1200
Connection ~ 7850 1200
Wire Wire Line
	7850 1200 7850 1300
Wire Wire Line
	8250 1200 8350 1200
Wire Wire Line
	8350 1200 8350 1300
$Comp
L Logic_LevelTranslator:TXS0108EPW U?
U 1 1 611F1434
P 1700 6650
AR Path="/611F1434" Ref="U?"  Part="1" 
AR Path="/611E7A25/611F1434" Ref="U1"  Part="1" 
F 0 "U1" H 1950 5950 50  0000 C CNN
F 1 "TXS0108EPW" H 2150 5850 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 1700 5900 50  0001 C CNN
F 3 "www.ti.com/lit/ds/symlink/txs0108e.pdf" H 1700 6550 50  0001 C CNN
	1    1700 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611F143A
P 1350 5850
AR Path="/611F143A" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F143A" Ref="C1"  Part="1" 
F 0 "C1" H 1050 5950 50  0000 L CNN
F 1 "0.1uF" H 1050 5850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1388 5700 50  0001 C CNN
F 3 "~" H 1350 5850 50  0001 C CNN
	1    1350 5850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611F1440
P 1100 5950
AR Path="/611F1440" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F1440" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 1100 5700 50  0001 C CNN
F 1 "GND" H 1105 5777 50  0000 C CNN
F 2 "" H 1100 5950 50  0001 C CNN
F 3 "" H 1100 5950 50  0001 C CNN
	1    1100 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611F1446
P 2050 5850
AR Path="/611F1446" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F1446" Ref="C2"  Part="1" 
F 0 "C2" H 1750 5950 50  0000 L CNN
F 1 "0.1uF" H 1750 5850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2088 5700 50  0001 C CNN
F 3 "~" H 2050 5850 50  0001 C CNN
	1    2050 5850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611F144C
P 2300 5950
AR Path="/611F144C" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F144C" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 2300 5700 50  0001 C CNN
F 1 "GND" H 2305 5777 50  0000 C CNN
F 2 "" H 2300 5950 50  0001 C CNN
F 3 "" H 2300 5950 50  0001 C CNN
	1    2300 5950
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 611F1452
P 1600 5600
AR Path="/611F1452" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F1452" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 1600 5450 50  0001 C CNN
F 1 "VDD" H 1615 5773 50  0000 C CNN
F 2 "" H 1600 5600 50  0001 C CNN
F 3 "" H 1600 5600 50  0001 C CNN
	1    1600 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 7350 1700 7450
$Comp
L power:GND #PWR?
U 1 1 611F145F
P 1700 7450
AR Path="/611F145F" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F145F" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 1700 7200 50  0001 C CNN
F 1 "GND" H 1705 7277 50  0000 C CNN
F 2 "" H 1700 7450 50  0001 C CNN
F 3 "" H 1700 7450 50  0001 C CNN
	1    1700 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 5600 1600 5850
Wire Wire Line
	1500 5850 1600 5850
Connection ~ 1600 5850
Wire Wire Line
	1600 5850 1600 5950
Wire Wire Line
	1200 5850 1100 5850
Wire Wire Line
	1100 5850 1100 5950
Wire Wire Line
	1800 5850 1900 5850
Connection ~ 1800 5850
Wire Wire Line
	1800 5850 1800 5950
Wire Wire Line
	2200 5850 2300 5850
Wire Wire Line
	2300 5850 2300 5950
Connection ~ 2950 950 
Wire Wire Line
	3200 950  2950 950 
Connection ~ 3550 950 
Wire Wire Line
	3550 950  3400 950 
Wire Wire Line
	4300 950  4300 1000
Wire Wire Line
	4050 950  4300 950 
Wire Wire Line
	3550 950  3750 950 
$Comp
L power:GND #PWR?
U 1 1 611F13D5
P 4300 1000
AR Path="/611F13D5" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F13D5" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 4300 750 50  0001 C CNN
F 1 "GND" H 4305 827 50  0000 C CNN
F 2 "" H 4300 1000 50  0001 C CNN
F 3 "" H 4300 1000 50  0001 C CNN
	1    4300 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611F13CF
P 3900 950
AR Path="/611F13CF" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F13CF" Ref="C5"  Part="1" 
F 0 "C5" H 3600 1050 50  0000 L CNN
F 1 "0.1uF" H 3600 950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3938 800 50  0001 C CNN
F 3 "~" H 3900 950 50  0001 C CNN
	1    3900 950 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 950  2850 950 
Wire Wire Line
	2350 950  2550 950 
Wire Wire Line
	2350 1050 2350 950 
Wire Wire Line
	2950 800  2950 950 
$Comp
L power:VDD #PWR?
U 1 1 611F13C5
P 2950 800
AR Path="/611F13C5" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F13C5" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 2950 650 50  0001 C CNN
F 1 "VDD" H 2965 973 50  0000 C CNN
F 2 "" H 2950 800 50  0001 C CNN
F 3 "" H 2950 800 50  0001 C CNN
	1    2950 800 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611F13BF
P 2350 1050
AR Path="/611F13BF" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F13BF" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 2350 800 50  0001 C CNN
F 1 "GND" H 2355 877 50  0000 C CNN
F 2 "" H 2350 1050 50  0001 C CNN
F 3 "" H 2350 1050 50  0001 C CNN
	1    2350 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 611F13B9
P 2700 950
AR Path="/611F13B9" Ref="C?"  Part="1" 
AR Path="/611E7A25/611F13B9" Ref="C3"  Part="1" 
F 0 "C3" H 2400 1050 50  0000 L CNN
F 1 "0.1uF" H 2400 950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2738 800 50  0001 C CNN
F 3 "~" H 2700 950 50  0001 C CNN
	1    2700 950 
	0    -1   -1   0   
$EndComp
$Comp
L local_logic_level_translator:SN74AXC4T774 D?
U 1 1 611F13B3
P 3300 1900
AR Path="/611F13B3" Ref="D?"  Part="1" 
AR Path="/611E7A25/611F13B3" Ref="D1"  Part="1" 
F 0 "D1" H 3550 1450 50  0000 C CNN
F 1 "SN74AXC4T774" H 3800 1350 50  0000 C CNN
F 2 "Package_SO:SSOP-16_4.4x5.2mm_P0.65mm" H 3450 2800 50  0001 C CNN
F 3 "" H 3450 2800 50  0001 C CNN
	1    3300 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 611F138E
P 7150 4250
AR Path="/611F138E" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611F138E" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 7150 4000 50  0001 C CNN
F 1 "GND" H 7155 4077 50  0000 C CNN
F 2 "" H 7150 4250 50  0001 C CNN
F 3 "" H 7150 4250 50  0001 C CNN
	1    7150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 950  3400 1400
Wire Wire Line
	3200 950  3200 1400
$Comp
L power:VDD #PWR?
U 1 1 6130EACE
P 2600 1550
AR Path="/6130EACE" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6130EACE" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 2600 1400 50  0001 C CNN
F 1 "VDD" H 2615 1723 50  0000 C CNN
F 2 "" H 2600 1550 50  0001 C CNN
F 3 "" H 2600 1550 50  0001 C CNN
	1    2600 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61351B2C
P 3300 2500
AR Path="/61351B2C" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/61351B2C" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 3300 2250 50  0001 C CNN
F 1 "GND" H 3305 2327 50  0000 C CNN
F 2 "" H 3300 2500 50  0001 C CNN
F 3 "" H 3300 2500 50  0001 C CNN
	1    3300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2400 3300 2500
Wire Wire Line
	2800 1600 2750 1600
Wire Wire Line
	2800 1700 2750 1700
Wire Wire Line
	2750 1700 2750 1600
Connection ~ 2750 1600
Wire Wire Line
	2750 1600 2600 1600
Wire Wire Line
	2600 1550 2600 1600
Connection ~ 2950 3050
Wire Wire Line
	3200 3050 2950 3050
Connection ~ 3550 3050
Wire Wire Line
	3550 3050 3400 3050
Wire Wire Line
	4300 3050 4300 3100
Wire Wire Line
	4050 3050 4300 3050
Wire Wire Line
	3550 3050 3750 3050
$Comp
L power:GND #PWR?
U 1 1 6118B152
P 4300 3100
AR Path="/6118B152" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6118B152" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 4300 2850 50  0001 C CNN
F 1 "GND" H 4305 2927 50  0000 C CNN
F 2 "" H 4300 3100 50  0001 C CNN
F 3 "" H 4300 3100 50  0001 C CNN
	1    4300 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6118B158
P 3900 3050
AR Path="/6118B158" Ref="C?"  Part="1" 
AR Path="/611E7A25/6118B158" Ref="C6"  Part="1" 
F 0 "C6" H 3600 3150 50  0000 L CNN
F 1 "0.1uF" H 3600 3050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3938 2900 50  0001 C CNN
F 3 "~" H 3900 3050 50  0001 C CNN
	1    3900 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 3050 2850 3050
Wire Wire Line
	2350 3050 2550 3050
Wire Wire Line
	2350 3150 2350 3050
Wire Wire Line
	2950 2900 2950 3050
$Comp
L power:VDD #PWR?
U 1 1 6118B162
P 2950 2900
AR Path="/6118B162" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6118B162" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 2950 2750 50  0001 C CNN
F 1 "VDD" H 2965 3073 50  0000 C CNN
F 2 "" H 2950 2900 50  0001 C CNN
F 3 "" H 2950 2900 50  0001 C CNN
	1    2950 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6118B168
P 2350 3150
AR Path="/6118B168" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6118B168" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 2350 2900 50  0001 C CNN
F 1 "GND" H 2355 2977 50  0000 C CNN
F 2 "" H 2350 3150 50  0001 C CNN
F 3 "" H 2350 3150 50  0001 C CNN
	1    2350 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6118B16E
P 2700 3050
AR Path="/6118B16E" Ref="C?"  Part="1" 
AR Path="/611E7A25/6118B16E" Ref="C4"  Part="1" 
F 0 "C4" H 2400 3150 50  0000 L CNN
F 1 "0.1uF" H 2400 3050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 2738 2900 50  0001 C CNN
F 3 "~" H 2700 3050 50  0001 C CNN
	1    2700 3050
	0    -1   -1   0   
$EndComp
$Comp
L local_logic_level_translator:SN74AXC4T774 D?
U 1 1 6118B174
P 3300 4000
AR Path="/6118B174" Ref="D?"  Part="1" 
AR Path="/611E7A25/6118B174" Ref="D2"  Part="1" 
F 0 "D2" H 3550 3550 50  0000 C CNN
F 1 "SN74AXC4T774" H 3800 3450 50  0000 C CNN
F 2 "Package_SO:SSOP-16_4.4x5.2mm_P0.65mm" H 3450 4900 50  0001 C CNN
F 3 "" H 3450 4900 50  0001 C CNN
	1    3300 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 3050 3400 3500
Wire Wire Line
	3200 3050 3200 3500
$Comp
L power:VDD #PWR?
U 1 1 6118B180
P 2600 3650
AR Path="/6118B180" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6118B180" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 2600 3500 50  0001 C CNN
F 1 "VDD" H 2615 3823 50  0000 C CNN
F 2 "" H 2600 3650 50  0001 C CNN
F 3 "" H 2600 3650 50  0001 C CNN
	1    2600 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6118B19F
P 3300 4600
AR Path="/6118B19F" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6118B19F" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 3300 4350 50  0001 C CNN
F 1 "GND" H 3305 4427 50  0000 C CNN
F 2 "" H 3300 4600 50  0001 C CNN
F 3 "" H 3300 4600 50  0001 C CNN
	1    3300 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6118B1A5
P 2350 4000
AR Path="/6118B1A5" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6118B1A5" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 2350 3750 50  0001 C CNN
F 1 "GND" H 2355 3827 50  0000 C CNN
F 2 "" H 2350 4000 50  0001 C CNN
F 3 "" H 2350 4000 50  0001 C CNN
	1    2350 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 4500 3300 4600
Wire Wire Line
	2800 3700 2750 3700
Wire Wire Line
	2800 3800 2750 3800
Wire Wire Line
	2750 3800 2750 3700
Connection ~ 2750 3700
Wire Wire Line
	2750 3700 2600 3700
Wire Wire Line
	2600 3650 2600 3700
Wire Wire Line
	3800 3700 3900 3700
Wire Wire Line
	4000 3700 4000 3650
Wire Wire Line
	3800 3800 3900 3800
Wire Wire Line
	3900 3700 3900 3800
Connection ~ 3900 3700
Wire Wire Line
	3900 3700 4000 3700
Text HLabel 1150 6350 0    50   Input ~ 0
PB0
Text HLabel 1150 6450 0    50   Input ~ 0
PB1
Text HLabel 1150 6550 0    50   Input ~ 0
PB2
Text HLabel 1150 6650 0    50   Input ~ 0
PB3
Text HLabel 1150 6750 0    50   Input ~ 0
PB4
Text HLabel 1150 6850 0    50   Input ~ 0
PB5
Text HLabel 1150 6950 0    50   Input ~ 0
PB6
Text HLabel 1150 7050 0    50   Input ~ 0
PB7
Wire Wire Line
	1300 6350 1150 6350
Wire Wire Line
	1150 6450 1300 6450
Wire Wire Line
	1300 6550 1150 6550
Wire Wire Line
	1150 6650 1300 6650
Wire Wire Line
	1300 6750 1150 6750
Wire Wire Line
	1150 6850 1300 6850
Wire Wire Line
	1300 6950 1150 6950
Wire Wire Line
	1150 7050 1300 7050
Text HLabel 7200 1700 0    50   Input ~ 0
PC0
Text HLabel 7200 1800 0    50   Input ~ 0
PC1
Text HLabel 7200 1900 0    50   Input ~ 0
PC2
Text HLabel 7200 2000 0    50   Input ~ 0
PC3
Text HLabel 7200 2100 0    50   Input ~ 0
PC4
Text HLabel 7200 2200 0    50   Input ~ 0
PC5
Text HLabel 7200 2300 0    50   Input ~ 0
PC6
Text HLabel 7200 2400 0    50   Input ~ 0
PC7
Wire Wire Line
	7350 1700 7200 1700
Wire Wire Line
	7200 1800 7350 1800
Wire Wire Line
	7350 1900 7200 1900
Wire Wire Line
	7200 2000 7350 2000
Wire Wire Line
	7350 2100 7200 2100
Wire Wire Line
	7200 2200 7350 2200
Wire Wire Line
	7350 2300 7200 2300
Wire Wire Line
	7200 2400 7350 2400
$Comp
L power:GND #PWR?
U 1 1 61161170
P 1800 1400
AR Path="/61161170" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/61161170" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 1800 1150 50  0001 C CNN
F 1 "GND" H 1805 1227 50  0000 C CNN
F 2 "" H 1800 1400 50  0001 C CNN
F 3 "" H 1800 1400 50  0001 C CNN
	1    1800 1400
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 611706AF
P 1000 1400
F 0 "#FLG0101" H 1000 1475 50  0001 C CNN
F 1 "PWR_FLAG" H 1000 1573 50  0000 C CNN
F 2 "" H 1000 1400 50  0001 C CNN
F 3 "~" H 1000 1400 50  0001 C CNN
	1    1000 1400
	-1   0    0    1   
$EndComp
Text GLabel 900  1300 0    50   Input ~ 0
Vjtag_in
$Comp
L power:VDD #PWR?
U 1 1 6118B186
P 4000 3650
AR Path="/6118B186" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6118B186" Ref="#PWR0130"  Part="1" 
F 0 "#PWR0130" H 4000 3500 50  0001 C CNN
F 1 "VDD" H 4015 3823 50  0000 C CNN
F 2 "" H 4000 3650 50  0001 C CNN
F 3 "" H 4000 3650 50  0001 C CNN
	1    4000 3650
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 61314D3E
P 4000 1450
AR Path="/61314D3E" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/61314D3E" Ref="#PWR0131"  Part="1" 
F 0 "#PWR0131" H 4000 1300 50  0001 C CNN
F 1 "VDD" H 4015 1623 50  0000 C CNN
F 2 "" H 4000 1450 50  0001 C CNN
F 3 "" H 4000 1450 50  0001 C CNN
	1    4000 1450
	1    0    0    -1  
$EndComp
Text GLabel 3650 700  2    50   Input ~ 0
Vjtag_in
Wire Wire Line
	3650 700  3550 700 
Wire Wire Line
	3550 700  3550 950 
Text GLabel 3650 2800 2    50   Input ~ 0
Vjtag_in
Wire Wire Line
	3650 2800 3550 2800
Wire Wire Line
	3550 2800 3550 3050
$Comp
L Device:R R1
U 1 1 6116E948
P 800 6050
F 0 "R1" H 870 6096 50  0000 L CNN
F 1 "10K" H 870 6005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 730 6050 50  0001 C CNN
F 3 "~" H 800 6050 50  0001 C CNN
	1    800  6050
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 6116EFD7
P 800 5750
AR Path="/6116EFD7" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/6116EFD7" Ref="#PWR0132"  Part="1" 
F 0 "#PWR0132" H 800 5600 50  0001 C CNN
F 1 "VDD" H 815 5923 50  0000 C CNN
F 2 "" H 800 5750 50  0001 C CNN
F 3 "" H 800 5750 50  0001 C CNN
	1    800  5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  5750 800  5900
Wire Wire Line
	800  6200 800  6250
Wire Wire Line
	800  6250 1300 6250
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 6117D757
P 3550 6550
F 0 "J2" H 3600 6967 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 3600 6876 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Horizontal" H 3550 6550 50  0001 C CNN
F 3 "~" H 3550 6550 50  0001 C CNN
	1    3550 6550
	1    0    0    -1  
$EndComp
Text Label 2400 6350 2    50   ~ 0
b0_out
Text Label 2400 6450 2    50   ~ 0
b1_out
Text Label 2400 6550 2    50   ~ 0
b2_out
Text Label 2400 6750 2    50   ~ 0
b4_out
Text Label 2400 6850 2    50   ~ 0
b5_out
Text Label 2400 6950 2    50   ~ 0
b6_out
Text Label 2400 7050 2    50   ~ 0
b7_out
Text Label 3050 6350 0    50   ~ 0
b0_out
Text Label 4150 6350 2    50   ~ 0
b1_out
Text Label 4150 6450 2    50   ~ 0
b3_out
Text Label 4150 6550 2    50   ~ 0
b5_out
Text Label 4150 6650 2    50   ~ 0
b7_out
Text Label 3050 6550 0    50   ~ 0
b4_out
Text Label 3050 6650 0    50   ~ 0
b6_out
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6115FE9A
P 1300 1100
F 0 "J1" H 1350 1517 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 1350 1426 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 1300 1100 50  0001 C CNN
F 3 "~" H 1300 1100 50  0001 C CNN
	1    1300 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 900  900  900 
Wire Wire Line
	900  1000 1100 1000
Wire Wire Line
	1100 1100 900  1100
Wire Wire Line
	900  1200 1100 1200
Wire Wire Line
	1600 900  1800 900 
Wire Wire Line
	1800 1000 1600 1000
Wire Wire Line
	1600 1100 1800 1100
Wire Wire Line
	1800 1200 1600 1200
Wire Wire Line
	900  1300 1000 1300
Wire Wire Line
	1000 1300 1000 1400
Connection ~ 1000 1300
Wire Wire Line
	1000 1300 1100 1300
Wire Wire Line
	1800 1400 1800 1300
Wire Wire Line
	1800 1300 1600 1300
Wire Wire Line
	2650 1900 2800 1900
Wire Wire Line
	2800 2000 2650 2000
Wire Wire Line
	2650 2100 2800 2100
Wire Wire Line
	2800 2200 2650 2200
$Comp
L power:GND #PWR?
U 1 1 61383203
P 2300 1850
AR Path="/61383203" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/61383203" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 2300 1600 50  0001 C CNN
F 1 "GND" H 2305 1677 50  0000 C CNN
F 2 "" H 2300 1850 50  0001 C CNN
F 3 "" H 2300 1850 50  0001 C CNN
	1    2300 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 1850 2300 1800
Wire Wire Line
	2300 1800 2800 1800
Wire Wire Line
	2650 4000 2800 4000
Wire Wire Line
	2800 4100 2650 4100
Wire Wire Line
	2650 4200 2800 4200
Wire Wire Line
	2800 4300 2650 4300
Wire Wire Line
	2800 3900 2350 3900
Wire Wire Line
	2350 3900 2350 4000
$Comp
L power:GND #PWR?
U 1 1 613D3997
P 5750 1700
AR Path="/613D3997" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/613D3997" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 5750 1450 50  0001 C CNN
F 1 "GND" H 5755 1527 50  0000 C CNN
F 2 "" H 5750 1700 50  0001 C CNN
F 3 "" H 5750 1700 50  0001 C CNN
	1    5750 1700
	1    0    0    -1  
$EndComp
Text GLabel 4750 1600 0    50   Input ~ 0
Vjtag_in
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J4
U 1 1 613D39A8
P 5200 1400
F 0 "J4" H 5250 1817 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5250 1726 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Horizontal" H 5200 1400 50  0001 C CNN
F 3 "~" H 5200 1400 50  0001 C CNN
	1    5200 1400
	1    0    0    -1  
$EndComp
Text Label 4750 1200 0    50   ~ 0
a0_out
Wire Wire Line
	5000 1200 4750 1200
Wire Wire Line
	5000 1300 4750 1300
Wire Wire Line
	5000 1400 4750 1400
Wire Wire Line
	4750 1500 5000 1500
Wire Wire Line
	5750 1200 5500 1200
Wire Wire Line
	5500 1300 5750 1300
Wire Wire Line
	5500 1400 5750 1400
Wire Wire Line
	5500 1500 5750 1500
Wire Wire Line
	5500 1600 5750 1600
Wire Wire Line
	5750 1600 5750 1700
Text Label 4750 1300 0    50   ~ 0
a2_out
Text Label 4750 1400 0    50   ~ 0
a4_out
Text Label 4750 1500 0    50   ~ 0
a6_out
Text Label 5750 1200 2    50   ~ 0
a1_out
Text Label 5750 1300 2    50   ~ 0
a3_out
Text Label 5750 1400 2    50   ~ 0
a5_out
Text Label 5750 1500 2    50   ~ 0
a7_out
$Comp
L power:GND #PWR?
U 1 1 614646E7
P 4150 1550
AR Path="/614646E7" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/614646E7" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 4150 1300 50  0001 C CNN
F 1 "GND" H 4155 1377 50  0000 C CNN
F 2 "" H 4150 1550 50  0001 C CNN
F 3 "" H 4150 1550 50  0001 C CNN
	1    4150 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1700 4000 1700
Wire Wire Line
	3800 1600 3900 1600
Wire Wire Line
	3900 1600 3900 1500
Wire Wire Line
	3900 1500 4150 1500
Wire Wire Line
	4000 1450 4000 1700
Wire Wire Line
	4150 1500 4150 1550
Text Label 4050 1900 2    50   ~ 0
a0_out
Wire Wire Line
	4050 1900 3800 1900
Wire Wire Line
	4050 2000 3800 2000
Wire Wire Line
	4050 2100 3800 2100
Wire Wire Line
	3800 2200 4050 2200
Text Label 4050 2000 2    50   ~ 0
a1_out
Text Label 4050 2100 2    50   ~ 0
a2_out
Text Label 4050 2200 2    50   ~ 0
a3_out
Wire Wire Line
	3800 4000 4050 4000
Wire Wire Line
	3800 4100 4050 4100
Wire Wire Line
	3800 4200 4050 4200
Wire Wire Line
	3800 4300 4050 4300
Text Label 4050 4000 2    50   ~ 0
a4_out
Text Label 4050 4100 2    50   ~ 0
a5_out
Text Label 4050 4200 2    50   ~ 0
a6_out
Text Label 4050 4300 2    50   ~ 0
a7_out
$Comp
L power:GND #PWR?
U 1 1 614F0C04
P 4650 3200
AR Path="/614F0C04" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/614F0C04" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 4650 2950 50  0001 C CNN
F 1 "GND" H 4655 3027 50  0000 C CNN
F 2 "" H 4650 3200 50  0001 C CNN
F 3 "" H 4650 3200 50  0001 C CNN
	1    4650 3200
	1    0    0    -1  
$EndComp
Text GLabel 5550 2450 2    50   Input ~ 0
Vjtag_in
Text Label 5550 2650 2    50   ~ 0
a0_out
Text Label 5550 2750 2    50   ~ 0
a2_out
Text Label 4800 4850 2    50   ~ 0
a4_out
Text Label 5550 2850 2    50   ~ 0
a1_out
Text Label 5550 2550 2    50   ~ 0
a3_out
Text Label 5550 3050 2    50   ~ 0
a7_out
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J3
U 1 1 614FBC76
P 4950 2750
F 0 "J3" H 5000 3267 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 5000 3176 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x07_P2.54mm_Horizontal" H 4950 2750 50  0001 C CNN
F 3 "~" H 4950 2750 50  0001 C CNN
	1    4950 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2450 4650 2550
Wire Wire Line
	4650 2450 4750 2450
Wire Wire Line
	4750 2550 4650 2550
Connection ~ 4650 2550
Wire Wire Line
	4650 2550 4650 2650
Wire Wire Line
	4650 2650 4750 2650
Connection ~ 4650 2650
Wire Wire Line
	4650 2650 4650 2750
Wire Wire Line
	4750 2750 4650 2750
Connection ~ 4650 2750
Wire Wire Line
	4650 2750 4650 2850
Wire Wire Line
	4650 2850 4750 2850
Connection ~ 4650 2850
Wire Wire Line
	4650 2850 4650 2950
Wire Wire Line
	4750 2950 4650 2950
Connection ~ 4650 2950
Wire Wire Line
	4650 2950 4650 3050
Wire Wire Line
	4650 3050 4750 3050
Wire Wire Line
	4650 3050 4650 3200
Connection ~ 4650 3050
Text Notes 4550 4350 0    50   ~ 0
Xilinx JTAG cable pinout\n(seen looking into the board conn)\n\n1  - GND   2  - VCC_IO \n3  - GND   4  - TMS    \n5  - GND   6  - TCK    \n7  - GND   8  - TDO    \n9  - GND   10 - TDI    \n11 - GND   12 - ----   \n13 - GND   14 - PS_SRST
Wire Notes Line
	4500 4400 5950 4400
Wire Wire Line
	5250 2450 5550 2450
Wire Wire Line
	5250 2550 5550 2550
Wire Wire Line
	5550 2650 5250 2650
Wire Wire Line
	5250 2750 5550 2750
Wire Wire Line
	5550 2850 5250 2850
Wire Wire Line
	5250 2950 5550 2950
Wire Wire Line
	5550 3050 5250 3050
NoConn ~ 5550 2950
$Comp
L Device:R R8
U 1 1 616FF8BD
P 5100 4850
F 0 "R8" V 4893 4850 50  0000 C CNN
F 1 "2K" V 4984 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5030 4850 50  0001 C CNN
F 3 "~" H 5100 4850 50  0001 C CNN
	1    5100 4850
	0    1    1    0   
$EndComp
Text GLabel 5400 4850 2    50   Input ~ 0
Vjtag_in
Wire Wire Line
	4800 4850 4950 4850
Wire Wire Line
	5250 4850 5400 4850
Text Notes 4550 5100 0    50   ~ 0
according to zcu104 scheme
Wire Notes Line
	4500 4550 5950 4550
Wire Notes Line
	5950 4550 5950 5150
Wire Notes Line
	5950 5150 4500 5150
Wire Notes Line
	4500 5150 4500 4550
Wire Notes Line
	4500 2150 5950 2150
Wire Notes Line
	4500 2150 4500 4400
Wire Notes Line
	5950 2150 5950 4400
Text HLabel 1800 900  2    50   Input ~ 0
PA1
Text HLabel 1800 1000 2    50   Input ~ 0
PA3
Text HLabel 1800 1100 2    50   Input ~ 0
PA5
Text HLabel 1800 1200 2    50   Input ~ 0
PA7
Text HLabel 900  900  0    50   Input ~ 0
PA0
Text HLabel 900  1000 0    50   Input ~ 0
PA2
Text HLabel 900  1100 0    50   Input ~ 0
PA4
Text HLabel 900  1200 0    50   Input ~ 0
PA6
Text HLabel 2650 1900 0    50   Input ~ 0
PA0
Text HLabel 2650 2000 0    50   Input ~ 0
PA1
Text HLabel 2650 2100 0    50   Input ~ 0
PA2
Text HLabel 2650 2200 0    50   Input ~ 0
PA3
Text HLabel 2650 4000 0    50   Input ~ 0
PA4
Text HLabel 2650 4100 0    50   Input ~ 0
PA5
Text HLabel 2650 4200 0    50   Input ~ 0
PA6
Text HLabel 2650 4300 0    50   Input ~ 0
PA7
$Comp
L Device:R R9
U 1 1 611E6A03
P 6850 1350
F 0 "R9" H 6920 1396 50  0000 L CNN
F 1 "R" H 6920 1305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6780 1350 50  0001 C CNN
F 3 "~" H 6850 1350 50  0001 C CNN
	1    6850 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1600 6850 1600
Wire Wire Line
	6850 1600 6850 1500
Wire Wire Line
	6850 1200 6850 1100
$Comp
L power:VDD #PWR?
U 1 1 611FD8C9
P 6850 1100
AR Path="/611FD8C9" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/611FD8C9" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 6850 950 50  0001 C CNN
F 1 "VDD" H 6865 1273 50  0000 C CNN
F 2 "" H 6850 1100 50  0001 C CNN
F 3 "" H 6850 1100 50  0001 C CNN
	1    6850 1100
	1    0    0    -1  
$EndComp
Text GLabel 1900 5450 2    50   Input ~ 0
Vpb_in
Wire Wire Line
	7900 750  7850 750 
Wire Wire Line
	7850 750  7850 1200
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J6
U 1 1 6120C9A5
P 9500 2050
F 0 "J6" H 9550 2467 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 9550 2376 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Horizontal" H 9500 2050 50  0001 C CNN
F 3 "~" H 9500 2050 50  0001 C CNN
	1    9500 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J7
U 1 1 6120F1F1
P 9600 4950
F 0 "J7" H 9650 5367 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 9650 5276 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Horizontal" H 9600 4950 50  0001 C CNN
F 3 "~" H 9600 4950 50  0001 C CNN
	1    9600 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 5450 1800 5450
Wire Wire Line
	1800 5450 1800 5850
Text GLabel 7900 750  2    50   Input ~ 0
Vpc_in
Text GLabel 3100 6850 3    50   Input ~ 0
Vpb_in
Wire Wire Line
	3050 6350 3350 6350
Wire Wire Line
	3050 6450 3350 6450
Wire Wire Line
	3050 6550 3350 6550
Wire Wire Line
	3050 6650 3350 6650
Text Label 3050 6450 0    50   ~ 0
b2_out
Wire Wire Line
	3850 6350 4150 6350
Wire Wire Line
	3850 6450 4150 6450
Wire Wire Line
	3850 6550 4150 6550
Wire Wire Line
	3850 6650 4150 6650
Text Label 2400 6650 2    50   ~ 0
b3_out
Wire Wire Line
	2100 6650 2400 6650
Wire Wire Line
	2100 6550 2400 6550
Wire Wire Line
	2100 6450 2400 6450
Wire Wire Line
	2100 6350 2400 6350
Wire Wire Line
	2100 6750 2400 6750
Wire Wire Line
	2100 6850 2400 6850
Wire Wire Line
	2100 6950 2400 6950
Wire Wire Line
	2100 7050 2400 7050
Entry Wire Line
	2400 6350 2500 6450
Entry Wire Line
	2400 6450 2500 6550
Entry Wire Line
	2400 6550 2500 6650
Entry Wire Line
	2400 6650 2500 6750
Entry Wire Line
	2400 6750 2500 6850
Entry Wire Line
	2400 6850 2500 6950
Entry Wire Line
	2400 6950 2500 7050
Entry Wire Line
	2400 7050 2500 7150
Entry Wire Line
	2950 6450 3050 6350
Entry Wire Line
	2950 6550 3050 6450
Entry Wire Line
	2950 6650 3050 6550
Entry Wire Line
	2950 6750 3050 6650
Entry Wire Line
	4150 6350 4250 6450
Entry Wire Line
	4150 6450 4250 6550
Entry Wire Line
	4150 6550 4250 6650
Entry Wire Line
	4150 6650 4250 6750
Text HLabel 7050 4650 0    50   Input ~ 0
PD0
Text HLabel 7050 4750 0    50   Input ~ 0
PD1
Text HLabel 7050 4850 0    50   Input ~ 0
PD2
Text HLabel 7050 4950 0    50   Input ~ 0
PD3
Text HLabel 7050 5050 0    50   Input ~ 0
PD4
Text HLabel 7050 5150 0    50   Input ~ 0
PD5
Text HLabel 7050 5250 0    50   Input ~ 0
PD6
Text HLabel 7050 5350 0    50   Input ~ 0
PD7
Wire Wire Line
	7350 4650 7050 4650
Wire Wire Line
	7050 4750 7350 4750
Wire Wire Line
	7350 4850 7050 4850
Wire Wire Line
	7050 4950 7350 4950
Wire Wire Line
	7350 5050 7050 5050
Wire Wire Line
	7050 5150 7350 5150
Wire Wire Line
	7350 5250 7050 5250
Wire Wire Line
	7050 5350 7350 5350
$Comp
L Device:R R10
U 1 1 61479AE7
P 6900 4350
F 0 "R10" H 6970 4396 50  0000 L CNN
F 1 "R" H 6970 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6830 4350 50  0001 C CNN
F 3 "~" H 6900 4350 50  0001 C CNN
	1    6900 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4500 6900 4550
Wire Wire Line
	6900 4550 7350 4550
Wire Wire Line
	6900 4200 6900 4000
$Comp
L power:VDD #PWR?
U 1 1 61493128
P 6900 4000
AR Path="/61493128" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/61493128" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 6900 3850 50  0001 C CNN
F 1 "VDD" H 6915 4173 50  0000 C CNN
F 2 "" H 6900 4000 50  0001 C CNN
F 3 "" H 6900 4000 50  0001 C CNN
	1    6900 4000
	1    0    0    -1  
$EndComp
Wire Bus Line
	2500 7250 4250 7250
$Comp
L power:GND #PWR?
U 1 1 614A7A16
P 4000 6800
AR Path="/614A7A16" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/614A7A16" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 4000 6550 50  0001 C CNN
F 1 "GND" H 4005 6627 50  0000 C CNN
F 2 "" H 4000 6800 50  0001 C CNN
F 3 "" H 4000 6800 50  0001 C CNN
	1    4000 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 6750 4000 6750
Wire Wire Line
	4000 6750 4000 6800
Text GLabel 8000 3750 2    50   Input ~ 0
Vpd_in
Wire Wire Line
	7850 3750 7850 4150
Wire Wire Line
	7850 3750 8000 3750
$Comp
L power:PWR_FLAG #FLG01
U 1 1 614FD65B
P 3350 6950
F 0 "#FLG01" H 3350 7025 50  0001 C CNN
F 1 "PWR_FLAG" H 3350 7123 50  0000 C CNN
F 2 "" H 3350 6950 50  0001 C CNN
F 3 "~" H 3350 6950 50  0001 C CNN
	1    3350 6950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 6750 3100 6850
Wire Wire Line
	3100 6750 3300 6750
Wire Wire Line
	3350 6850 3300 6850
Wire Wire Line
	3300 6850 3300 6750
Connection ~ 3300 6750
Wire Wire Line
	3300 6750 3350 6750
Wire Wire Line
	3350 6850 3350 6950
Text Label 8500 1700 2    50   ~ 0
pc0_out
Wire Wire Line
	8150 1700 8500 1700
Wire Wire Line
	8150 1800 8500 1800
Wire Wire Line
	8150 1900 8500 1900
Wire Wire Line
	8150 2000 8500 2000
Wire Wire Line
	8150 2100 8500 2100
Wire Wire Line
	8150 2200 8500 2200
Wire Wire Line
	8150 2300 8500 2300
Wire Wire Line
	8150 2400 8500 2400
Text Label 8500 1800 2    50   ~ 0
pc1_out
Text Label 8500 1900 2    50   ~ 0
pc2_out
Text Label 8500 2000 2    50   ~ 0
pc3_out
Text Label 8500 2100 2    50   ~ 0
pc4_out
Text Label 8500 2200 2    50   ~ 0
pc5_out
Text Label 8500 2300 2    50   ~ 0
pc6_out
Text Label 8500 2400 2    50   ~ 0
pc7_out
Wire Wire Line
	9300 1850 8950 1850
Text Label 8950 1850 0    50   ~ 0
pc0_out
Wire Wire Line
	8950 1950 9300 1950
Wire Wire Line
	8950 2050 9300 2050
Wire Wire Line
	9300 2150 8950 2150
Text Label 8950 1950 0    50   ~ 0
pc2_out
Wire Wire Line
	10150 1850 9800 1850
Wire Wire Line
	9800 1950 10150 1950
Wire Wire Line
	10150 2050 9800 2050
Wire Wire Line
	9800 2150 10150 2150
Text Label 10150 1950 2    50   ~ 0
pc3_out
Text Label 8950 2050 0    50   ~ 0
pc4_out
Text Label 8950 2150 0    50   ~ 0
pc6_out
Text Label 10150 1850 2    50   ~ 0
pc1_out
Text Label 10150 2050 2    50   ~ 0
pc5_out
Text Label 10150 2150 2    50   ~ 0
pc7_out
Entry Wire Line
	8500 1700 8600 1800
Entry Wire Line
	8500 1800 8600 1900
Entry Wire Line
	8500 1900 8600 2000
Entry Wire Line
	8500 2000 8600 2100
Entry Wire Line
	8500 2100 8600 2200
Entry Wire Line
	8500 2200 8600 2300
Entry Wire Line
	8500 2300 8600 2400
Entry Wire Line
	8500 2400 8600 2500
Entry Wire Line
	8850 1950 8950 1850
Entry Wire Line
	8850 2050 8950 1950
Entry Wire Line
	8850 2150 8950 2050
Entry Wire Line
	8850 2250 8950 2150
Entry Wire Line
	10150 2150 10250 2250
Entry Wire Line
	10150 2050 10250 2150
Entry Wire Line
	10150 1950 10250 2050
Entry Wire Line
	10150 1850 10250 1950
Wire Bus Line
	8600 2650 10250 2650
Text GLabel 8950 2300 3    50   Input ~ 0
Vpc_in
$Comp
L power:PWR_FLAG #FLG03
U 1 1 616AF7CB
P 9200 2400
F 0 "#FLG03" H 9200 2475 50  0001 C CNN
F 1 "PWR_FLAG" H 9200 2573 50  0000 C CNN
F 2 "" H 9200 2400 50  0001 C CNN
F 3 "~" H 9200 2400 50  0001 C CNN
	1    9200 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	9300 2250 9200 2250
Wire Wire Line
	8950 2250 8950 2300
Wire Wire Line
	9200 2400 9200 2250
Connection ~ 9200 2250
Wire Wire Line
	9200 2250 8950 2250
Wire Wire Line
	9800 2250 9950 2250
Wire Wire Line
	9950 2250 9950 2400
$Comp
L power:GND #PWR?
U 1 1 616EF61D
P 9950 2400
AR Path="/616EF61D" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/616EF61D" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 9950 2150 50  0001 C CNN
F 1 "GND" H 9955 2227 50  0000 C CNN
F 2 "" H 9950 2400 50  0001 C CNN
F 3 "" H 9950 2400 50  0001 C CNN
	1    9950 2400
	1    0    0    -1  
$EndComp
Text Label 8500 4650 2    50   ~ 0
pd0_out
Wire Wire Line
	8150 4650 8500 4650
Wire Wire Line
	8150 4750 8500 4750
Wire Wire Line
	8150 4850 8500 4850
Wire Wire Line
	8150 4950 8500 4950
Wire Wire Line
	8150 5050 8500 5050
Wire Wire Line
	8150 5150 8500 5150
Wire Wire Line
	8150 5250 8500 5250
Wire Wire Line
	8150 5350 8500 5350
Text Label 8500 4750 2    50   ~ 0
pd1_out
Text Label 8500 4850 2    50   ~ 0
pd2_out
Text Label 8500 4950 2    50   ~ 0
pd3_out
Text Label 8500 5050 2    50   ~ 0
pd4_out
Text Label 8500 5150 2    50   ~ 0
pd5_out
Text Label 8500 5250 2    50   ~ 0
pd6_out
Text Label 8500 5350 2    50   ~ 0
pd7_out
Text Label 9000 4750 0    50   ~ 0
pd0_out
Text Label 10300 4750 2    50   ~ 0
pd1_out
Wire Wire Line
	9400 4750 9000 4750
Wire Wire Line
	9900 4750 10300 4750
Text Label 9000 4850 0    50   ~ 0
pd2_out
Text Label 9000 4950 0    50   ~ 0
pd4_out
Text Label 9000 5050 0    50   ~ 0
pd6_out
Text Label 10300 4850 2    50   ~ 0
pd3_out
Text Label 10300 4950 2    50   ~ 0
pd5_out
Text Label 10300 5050 2    50   ~ 0
pd7_out
Wire Wire Line
	9900 4850 10300 4850
Wire Wire Line
	10300 4950 9900 4950
Wire Wire Line
	9900 5050 10300 5050
Wire Wire Line
	9400 4850 9000 4850
Wire Wire Line
	9000 4950 9400 4950
Wire Wire Line
	9400 5050 9000 5050
Entry Wire Line
	8500 4650 8600 4750
Entry Wire Line
	8500 4750 8600 4850
Entry Wire Line
	8500 4850 8600 4950
Entry Wire Line
	8500 4950 8600 5050
Entry Wire Line
	8500 5050 8600 5150
Entry Wire Line
	8500 5150 8600 5250
Entry Wire Line
	8500 5250 8600 5350
Entry Wire Line
	8500 5350 8600 5450
Entry Wire Line
	8900 4850 9000 4750
Entry Wire Line
	8900 4950 9000 4850
Entry Wire Line
	8900 5050 9000 4950
Entry Wire Line
	8900 5150 9000 5050
Entry Wire Line
	10300 4750 10400 4850
Entry Wire Line
	10300 4850 10400 4950
Entry Wire Line
	10300 4950 10400 5050
Entry Wire Line
	10300 5050 10400 5150
Wire Bus Line
	8600 5600 10400 5600
Text GLabel 9050 5250 3    50   Input ~ 0
Vpd_in
$Comp
L power:PWR_FLAG #FLG04
U 1 1 6185BD9F
P 9300 5350
F 0 "#FLG04" H 9300 5425 50  0001 C CNN
F 1 "PWR_FLAG" H 9300 5523 50  0000 C CNN
F 2 "" H 9300 5350 50  0001 C CNN
F 3 "~" H 9300 5350 50  0001 C CNN
	1    9300 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	9400 5150 9300 5150
Wire Wire Line
	9050 5150 9050 5250
Wire Wire Line
	9300 5350 9300 5150
Connection ~ 9300 5150
Wire Wire Line
	9300 5150 9050 5150
Wire Wire Line
	9900 5150 10150 5150
$Comp
L power:GND #PWR?
U 1 1 618A8323
P 10150 5350
AR Path="/618A8323" Ref="#PWR?"  Part="1" 
AR Path="/611E7A25/618A8323" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 10150 5100 50  0001 C CNN
F 1 "GND" H 10155 5177 50  0000 C CNN
F 2 "" H 10150 5350 50  0001 C CNN
F 3 "" H 10150 5350 50  0001 C CNN
	1    10150 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 5150 10150 5350
Wire Wire Line
	4750 1600 5000 1600
Wire Bus Line
	2950 6350 2950 7250
Wire Bus Line
	8850 1800 8850 2650
Wire Bus Line
	8900 4750 8900 5600
Wire Bus Line
	10400 4750 10400 5600
Wire Bus Line
	4250 6350 4250 7250
Wire Bus Line
	10250 1800 10250 2650
Wire Bus Line
	2500 6350 2500 7250
Wire Bus Line
	8600 1800 8600 2650
Wire Bus Line
	8600 4750 8600 5600
$EndSCHEMATC
